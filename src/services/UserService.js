import axios from 'axios';

const users = {
  async login(data) {
    try {
      const res = await axios.post(`${process.env.VUE_APP_API_URL}/users/login`, data);
      if (res.status == 200 ) {
        localStorage.setItem('user', JSON.stringify(res.data));
      } else {return res.data}
    } catch (error) {
      console.log(error)
    }
  }
}

export default users;